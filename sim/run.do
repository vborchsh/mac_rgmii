.main clear

quietly set RTL_DIR "./../rtl"
quietly set TB_DIR  "./../tb"

vlog +initreg+0 +initmem+0 -sv -mfcu \
${TB_DIR}/tb_rgmii.sv                \
${RTL_DIR}/rgmii_top.sv              \
${RTL_DIR}/rgmii_tx.sv               \
${RTL_DIR}/rgmii_rx.sv               \
${RTL_DIR}/rgmii_mgb_ctrl.sv         \
${RTL_DIR}/rgmii_mgb_rxtx.sv         \
${RTL_DIR}/rgmii_sink.sv             \
${RTL_DIR}/rgmii_src.sv

restart -all -force
log -r /*
