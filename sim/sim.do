.main clear

set QUARTUS_ROOTDIR $env(QUARTUS_ROOTDIR)

quietly set RTL_DIR "./../rtl"
quietly set TB_DIR  "./../tb"

if {![file exists altera_mf_ver]} {
  vlib altera_mf_ver
  vlog -work altera_mf_ver $QUARTUS_ROOTDIR/eda/sim_lib/altera_mf.v
}

vlog +initreg+0 +initmem+0 -sv -mfcu \
${TB_DIR}/tb_rgmii.sv                \
${RTL_DIR}/rgmii_top.sv              \
${RTL_DIR}/rgmii_tx.sv               \
${RTL_DIR}/rgmii_rx.sv               \
${RTL_DIR}/rgmii_mgb_ctrl.sv         \
${RTL_DIR}/rgmii_mgb_rxtx.sv         \
${RTL_DIR}/rgmii_sink.sv             \
${RTL_DIR}/rgmii_src.sv

vsim -suppress 12110 -voptargs="+acc" +initreg+0 +initmem+0 -L altera_mf_ver tb_rgmii
