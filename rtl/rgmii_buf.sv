
//
// Project       : RGMII core
// Author        : Borchsh Vladislav
// Contacts      : borsch.v@micran.ru
// Workfile      : rgmii_buf.sv
// Description   : Cross-clock buffer for input/output data
//

// TODO: Delete Altera core for cross-platform

module rgmii_buf
  #(
    parameter pWORDS = "ext"
  )
  (
    input                iclkin  ,
    input                iclkout ,
    input                irst    ,
    input                ireq    , // Request data from FIFO
    // Input packet (synced with iclkin)
    input                isop    ,
    input                iena    ,
    input          [7:0] idat    ,
    input                ieop    ,
    // Service (synced with iclkout)
    output               oerr    , // Bad packet (<64 | >1512 bytes)
    output               ordy    ,
    // Output packet (synced with iclkout)
    output               osop    ,
    output               oena    ,
    output         [7:0] odat    ,
    output               oeop
  );
  
  //--------------------------------------------------------------------------------------------------------
  // Declaration parameters
  //--------------------------------------------------------------------------------------------------------
  
  localparam pWDAT = $size(idat);
  localparam pWADR = $clog2(pWORDS);

  //--------------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------------

  logic           [7:0] rst;
  logic                 eop;
  logic           [1:0] feop;
  logic                 dfeop;
  logic                 empty_wr;
  logic                 empty_rd;
  logic                 rd_ena;
  logic                 rd_rdy;
  logic                 rd_req;
  logic                 out_uflow;
  logic                 out_oflow;
  logic                 miss_pack;
  logic                 in_err;
  logic                 pack_err;
  logic                 empty;
  logic           [7:0] fdat;

  //--------------------------------------------------------------------------------------------------------
  // Declaration modules wires
  //--------------------------------------------------------------------------------------------------------
  
  logic   [pWDAT-1:0] fifo__data    ;
  logic               fifo__rdclk   ;
  logic               fifo__rdreq   ;
  logic               fifo__wrclk   ;
  logic               fifo__wrreq   ;
  logic               fifo__wrfull  ;
  logic               fifo__rdfull  ;
  logic   [pWDAT-1:0] fifo__q       ;
  logic               fifo__rdempty ;
  logic               fifo__wrempty ;
  logic               fifo__aclr    ;
  logic   [pWADR-1:0] fifo__wrusedw ;
  logic   [pWADR-1:0] fifo__rdusedw ;

  //--------------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------------

  // Crossclock triggers
  always_ff@(posedge iclkin) begin
    if (ieop)          eop <= 1'b1;
    else if (feop[1])  eop <= 1'b0;
  end
  always_ff@(posedge iclkout)  feop <= feop << 1 | eop;

  // Bad packets detection
  always_ff@(posedge iclkin) begin
    out_uflow <= ieop && (fifo__wrusedw < 62);   // Runt
    out_oflow <= ieop && (fifo__wrusedw > 1518); // Jumbo?
    miss_pack <= 1'b0;//isop && (~fifo__wrempty);       // Missed part of packet
  end
  assign in_err = (out_uflow | out_oflow | miss_pack);

  // Delayed reset after transmitt each packet
  always_ff@(posedge iclkout) rst <= rst << 1 | oeop;

  // Read after load full packet
  always_ff@(posedge iclkout) begin
    dfeop <= feop[1];
    //
    if (dfeop)               rd_ena <= (fifo__wrusedw >= 63);
    else if (fifo__rdempty)  rd_ena <= '0;
    //
    rd_req <= rd_ena & ireq;
    rd_rdy <= rd_req & ~empty_rd;
  end

  // Sync signals to output clock
  always_ff@(posedge iclkin)  empty_wr <= fifo__wrempty;
  always_ff@(posedge iclkout) begin
    empty_rd <= fifo__rdempty;
    empty    <= empty_wr & empty_rd;
    fdat     <= fifo__q;
    pack_err <= in_err;
  end

  // synthesis translate_off
  always_ff@(posedge iclkin) begin
    if (fifo__wrfull & fifo__wrreq) begin
      $display("!!! WARNING. FIFO OVERFLOW !!!");
      $display("%m");
    end
  end
  // synthesis translate_on

  //--------------------------------------------------------------------------------------------------------
  // Output data
  //--------------------------------------------------------------------------------------------------------
  
  assign ordy = empty;
  assign oerr = pack_err;
  assign osop = feop[0] & ~feop[1];
  assign oena = rd_rdy;
  assign oeop = rd_rdy & empty_rd;
  assign odat = fdat;
  
  //--------------------------------------------------------------------------------------------------------
  // Modules
  //--------------------------------------------------------------------------------------------------------

  // Crossclock FIFO
  assign fifo__aclr  = irst | in_err | rst[$high(rst)];
  assign fifo__wrclk = iclkin;
  assign fifo__wrreq = iena;
  assign fifo__data  = idat;
  assign fifo__rdclk = iclkout;
  assign fifo__rdreq = rd_ena & ireq;

  dcfifo
  #(
    . intended_device_family   ("Cyclone V"          ) ,
    . lpm_hint                 ("RAM_BLOCK_TYPE=M10K") ,
    . lpm_numwords             (pWORDS               ) ,
    . lpm_showahead            ("OFF"                ) ,
    . lpm_type                 ("dcfifo"             ) ,
    . lpm_width                (pWDAT                ) ,
    . lpm_widthu               (pWADR                ) ,
    . overflow_checking        ("ON"                 ) ,
    . rdsync_delaypipe         (4                    ) ,
    . read_aclr_synch          ("ON"                 ) ,
    . underflow_checking       ("ON"                 ) ,
    . use_eab                  ("ON"                 ) ,
    . write_aclr_synch         ("ON"                 ) ,
    . wrsync_delaypipe         (4                    )
  )
  fifo__
  (
    . aclr                     (fifo__aclr           ) ,
    . data                     (fifo__data           ) ,
    . rdclk                    (fifo__rdclk          ) ,
    . rdreq                    (fifo__rdreq          ) ,
    . wrclk                    (fifo__wrclk          ) ,
    . wrreq                    (fifo__wrreq          ) ,
    . q                        (fifo__q              ) ,
    . rdempty                  (fifo__rdempty        ) ,
    . wrfull                   (fifo__wrfull         ) ,
    . rdfull                   (fifo__rdfull         ) ,
    . rdusedw                  (fifo__rdusedw        ) ,
    . wrempty                  (fifo__wrempty        ) ,
    . wrusedw                  (fifo__wrusedw        ) ,
    . eccstatus                ()
  );

endmodule
