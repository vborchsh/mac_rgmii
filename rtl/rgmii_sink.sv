
//
// Project       : RGMII core
// Author        : Borchsh Vladislav
// Contacts      : borsch.v@micran.ru
// Workfile      : rgmii_sink.sv
// Description   : Cross-clock buffer for output data
//

module rgmii_sink
  #(
    parameter pWORDS = "ext"
  )
  (
    input                iclkin  ,
    input                iclkout ,
    input                irst    ,
    input                ireq    , // Request data from FIFO
    // Input packet (synced with iclkin)
    input                isop    ,
    input                iena    ,
    input          [7:0] idat    ,
    input                ieop    ,
    // Service (synced with iclkout)
    output               oerr    , // Overflow
    output               ordy    ,
    // Output packet (synced with iclkout)
    output               osop    ,
    output               oena    ,
    output         [7:0] odat    ,
    output               oeop
  );
  
  //--------------------------------------------------------------------------------------------------------
  // Declaration parameters
  //--------------------------------------------------------------------------------------------------------
  
  localparam pWDAT = 8+1+1;
  localparam pWADR = $clog2(pWORDS);

  //--------------------------------------------------------------------------------------------------------
  // Declaration modules wires
  //--------------------------------------------------------------------------------------------------------
  
  logic   [pWDAT-1:0] fifo__data    ;
  logic               fifo__rdclk   ;
  logic               fifo__rdreq   ;
  logic               fifo__wrclk   ;
  logic               fifo__wrreq   ;
  logic               fifo__wrfull  ;
  logic               fifo__rdfull  ;
  logic   [pWDAT-1:0] fifo__q       ;
  logic               fifo__rdempty ;
  logic               fifo__wrempty ;
  logic               fifo__aclr    ;
  logic   [pWADR-1:0] fifo__wrusedw ;
  logic   [pWADR-1:0] fifo__rdusedw ;

  //--------------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------------
  logic                 rd_ena;
  logic                 rd_rdy;
  logic                 rd_dat;
  logic           [2:0] cnt;
  logic           [1:0] eof;
  logic           [1:0] tail;
  logic                 sop;
  logic           [2:0] eop;
  logic           [7:0] dat;
  logic                 deop;
  logic                 wr_ena;
  logic           [9:0] wr_dat;

  //--------------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------------

  always_ff@(posedge iclkin) begin
    deop   <= ieop; // one word more for correct enable signals
    wr_ena <= (isop | ieop | iena | deop);
    wr_dat <= (deop) ? ('0) : ({isop, ieop, idat});
  end

  always@(posedge iclkin) begin
    if (ieop)         eop[0] <= 1'b1;
    else if (eop[1])  eop[0] <= 1'b0;
    eop[1] <= eop[0];
    eop[2] <= ~eop[1] & eop[0];
  end

  always@(posedge iclkout) begin
    eof    <= eof << 1 | eop[2];
    rd_ena <= ireq & ~fifo__rdempty & (cnt > 0) & ~oeop;
    rd_rdy <= rd_ena;
    rd_dat <= rd_rdy;

    if (rd_rdy)  dat <= fifo__q[7:0];

    if (~eof[1] & eof[0])  cnt <= cnt + 1'b1;
    else if (oeop)         cnt <= (cnt > 0) ? (cnt - 1'b1) : (cnt);

    tail <= tail << 1 | oeop; // 2 ticks for mask oena signal
    sop <= fifo__q[9] & rd_rdy;
  end

  // synthesis translate_off
  always_ff@(posedge iclkin) begin
    if (fifo__wrfull & fifo__wrreq) begin
      $display("!!! WARNING. FIFO OVERFLOW !!!");
      $display("%m");
    end
  end
  // synthesis translate_on

  //--------------------------------------------------------------------------------------------------------
  // Output data
  //--------------------------------------------------------------------------------------------------------
  
  assign ordy = fifo__wrempty;
  assign oerr = (fifo__wrfull & fifo__wrreq);
  assign osop = sop;
  assign oena = (rd_dat) & (~sop) & (~|tail);
  assign oeop = (fifo__q[8] & rd_rdy);
  assign odat = dat;
  
  //--------------------------------------------------------------------------------------------------------
  // Modules
  //--------------------------------------------------------------------------------------------------------

  assign fifo__aclr  = irst;
  assign fifo__wrclk = iclkin;
  assign fifo__wrreq = wr_ena;
  assign fifo__data  = wr_dat;
  assign fifo__rdclk = iclkout;
  assign fifo__rdreq = rd_ena;

  dcfifo
  #(
    . intended_device_family   ("Cyclone V"          ) ,
    . lpm_hint                 ("RAM_BLOCK_TYPE=M10K") ,
    . lpm_numwords             (pWORDS               ) ,
    . lpm_showahead            ("OFF"                ) ,
    . lpm_type                 ("dcfifo"             ) ,
    . lpm_width                (pWDAT                ) ,
    . lpm_widthu               (pWADR                ) ,
    . overflow_checking        ("ON"                 ) ,
    . rdsync_delaypipe         (4                    ) ,
    . read_aclr_synch          ("ON"                 ) ,
    . underflow_checking       ("ON"                 ) ,
    . use_eab                  ("ON"                 ) ,
    . write_aclr_synch         ("ON"                 ) ,
    . wrsync_delaypipe         (4                    )
  )
  fifo__
  (
    . aclr                     (fifo__aclr           ) ,
    . data                     (fifo__data           ) ,
    . rdclk                    (fifo__rdclk          ) ,
    . rdreq                    (fifo__rdreq          ) ,
    . wrclk                    (fifo__wrclk          ) ,
    . wrreq                    (fifo__wrreq          ) ,
    . q                        (fifo__q              ) ,
    . rdempty                  (fifo__rdempty        ) ,
    . wrfull                   (fifo__wrfull         ) ,
    . rdfull                   (fifo__rdfull         ) ,
    . rdusedw                  (fifo__rdusedw        ) ,
    . wrempty                  (fifo__wrempty        ) ,
    . wrusedw                  (fifo__wrusedw        ) ,
    . eccstatus                ()
  );

endmodule
