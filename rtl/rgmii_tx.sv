
//
// Project       : RGMII core
// Author        : Borchsh Vladislav
// Contacts      : borsch.v@micran.ru
// Workfile      : rgmii_tx.sv
// Description   : Transmitting module of RGMII interface
//

module rgmii_tx
  #(
    parameter pUSE_CRC = "ext"
  )
  (
    input                iclk   ,
    input                iclk_tx,
    input                irst   ,
    //
    input                isop   ,
    input                iena   ,
    input          [7:0] idat   ,
    input                ieop   ,
    //
    output               oena   ,
    output         [3:0] odat   ,
    output               oclk   ,
    //
    output               obsy   ,
    output               oreq
  );
  
  //--------------------------------------------------------------------------------------------------------
  // Declaration parameters
  //--------------------------------------------------------------------------------------------------------

  localparam pSFD    = 8'hD5;
  localparam pW_DDIO = 5;

  function integer eth_crc32_8d(input integer crc32, input logic [7:0] data);
    logic  [7:0] d;
    logic [31:0] c;
    logic [31:0] newcrc;
    
    begin
      for (int i=0; i<8; i++)   d[i] = data[7-i];

      c = crc32;

      newcrc[0]  = d[6] ^ d[0]  ^ c[24] ^ c[30];
      newcrc[1]  = d[7] ^ d[6]  ^ d[1]  ^ d[0]  ^ c[24] ^ c[25] ^ c[30] ^ c[31];
      newcrc[2]  = d[7] ^ d[6]  ^ d[2]  ^ d[1]  ^ d[0]  ^ c[24] ^ c[25] ^ c[26] ^ c[30] ^ c[31];
      newcrc[3]  = d[7] ^ d[3]  ^ d[2]  ^ d[1]  ^ c[25] ^ c[26] ^ c[27] ^ c[31];
      newcrc[4]  = d[6] ^ d[4]  ^ d[3]  ^ d[2]  ^ d[0]  ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
      newcrc[5]  = d[7] ^ d[6]  ^ d[5]  ^ d[4]  ^ d[3]  ^ d[1]  ^ d[0]  ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
      newcrc[6]  = d[7] ^ d[6]  ^ d[5]  ^ d[4]  ^ d[2]  ^ d[1]  ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
      newcrc[7]  = d[7] ^ d[5]  ^ d[3]  ^ d[2]  ^ d[0]  ^ c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
      newcrc[8]  = d[4] ^ d[3]  ^ d[1]  ^ d[0]  ^ c[0]  ^ c[24] ^ c[25] ^ c[27] ^ c[28];
      newcrc[9]  = d[5] ^ d[4]  ^ d[2]  ^ d[1]  ^ c[1]  ^ c[25] ^ c[26] ^ c[28] ^ c[29];
      newcrc[10] = d[5] ^ d[3]  ^ d[2]  ^ d[0]  ^ c[2]  ^ c[24] ^ c[26] ^ c[27] ^ c[29];
      newcrc[11] = d[4] ^ d[3]  ^ d[1]  ^ d[0]  ^ c[3]  ^ c[24] ^ c[25] ^ c[27] ^ c[28];
      newcrc[12] = d[6] ^ d[5]  ^ d[4]  ^ d[2]  ^ d[1]  ^ d[0]  ^ c[4]  ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
      newcrc[13] = d[7] ^ d[6]  ^ d[5]  ^ d[3]  ^ d[2]  ^ d[1]  ^ c[5]  ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
      newcrc[14] = d[7] ^ d[6]  ^ d[4]  ^ d[3]  ^ d[2]  ^ c[6]  ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
      newcrc[15] = d[7] ^ d[5]  ^ d[4]  ^ d[3]  ^ c[7]  ^ c[27] ^ c[28] ^ c[29] ^ c[31];
      newcrc[16] = d[5] ^ d[4]  ^ d[0]  ^ c[8]  ^ c[24] ^ c[28] ^ c[29];
      newcrc[17] = d[6] ^ d[5]  ^ d[1]  ^ c[9]  ^ c[25] ^ c[29] ^ c[30];
      newcrc[18] = d[7] ^ d[6]  ^ d[2]  ^ c[10] ^ c[26] ^ c[30] ^ c[31];
      newcrc[19] = d[7] ^ d[3]  ^ c[11] ^ c[27] ^ c[31];
      newcrc[20] = d[4] ^ c[12] ^ c[28] ;
      newcrc[21] = d[5] ^ c[13] ^ c[29] ;
      newcrc[22] = d[0] ^ c[14] ^ c[24] ;
      newcrc[23] = d[6] ^ d[1]  ^ d[0]  ^ c[15] ^ c[24] ^ c[25] ^ c[30];
      newcrc[24] = d[7] ^ d[2]  ^ d[1]  ^ c[16] ^ c[25] ^ c[26] ^ c[31];
      newcrc[25] = d[3] ^ d[2]  ^ c[17] ^ c[26] ^ c[27];
      newcrc[26] = d[6] ^ d[4]  ^ d[3]  ^ d[0]  ^ c[18] ^ c[24] ^ c[27] ^ c[28] ^ c[30];
      newcrc[27] = d[7] ^ d[5]  ^ d[4]  ^ d[1]  ^ c[19] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
      newcrc[28] = d[6] ^ d[5]  ^ d[2]  ^ c[20] ^ c[26] ^ c[29] ^ c[30];
      newcrc[29] = d[7] ^ d[6]  ^ d[3]  ^ c[21] ^ c[27] ^ c[30] ^ c[31];
      newcrc[30] = d[7] ^ d[4]  ^ c[22] ^ c[28] ^ c[31];
      newcrc[31] = d[5] ^ c[23] ^ c[29];

      eth_crc32_8d = newcrc;
    end
  endfunction
  
  //--------------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------------
  
  logic         [7:0] predat   ;
  logic         [7:0] ddat     ;
  logic         [3:0] cnt_req  ;
  logic         [3:0] cnt_pause;
  logic         [4:0] cnt_pream;
  logic         [1:0] cnt_crc  ;
  logic        [31:0] crc      ;
  logic        [15:0] cnt_dat  ;
  logic         [7:0] crc_inv  ;
  logic               ena      ;
  logic               dena     ;
  logic               sdr_cnt  ;
  logic               req      ;
  logic         [3:0] adat     ;
  logic               feop     ;
  
  enum bit [2:0]
  {
    StIdle   = 3'd0 ,
    StPream  = 3'd1 ,
    StDat    = 3'd2 ,
    StCRC    = 3'd3 ,
    StIPG    = 3'd4
  } St = StIdle /* synthesis syn_encoding = "safe, sequential" */;

  //--------------------------------------------------------------------------------------------------------
  // Declaration modules wires
  //--------------------------------------------------------------------------------------------------------

  logic                    ddio__aclr        ;
  logic                    ddio__outclock    ;
  logic      [pW_DDIO-1:0] ddio__dat_h       ;
  logic      [pW_DDIO-1:0] ddio__dat_l       ;
  logic      [pW_DDIO-1:0] ddio__dataout     ;

  logic                    ddio_clk__aclr    ;
  logic                    ddio_clk__outclock;
  logic                    ddio_clk__dat_h   ;
  logic                    ddio_clk__dat_l   ;
  logic                    ddio_clk__dataout ;

  //--------------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------------
  
  always_ff@(posedge iclk) begin
    feop <= ieop;
    if (irst)                                 St <= StIdle;
    else begin
      case (St)
        StIdle    : if (isop)                 St <= StPream;
        StPream   : if (cnt_pream == 5'd16)   St <= StDat;
        StDat     : if (feop)                 St <= (pUSE_CRC == "true") ? (StCRC) : (StIPG);
                    else if (cnt_dat > 3500)  St <= StIPG;
        StCRC     : if (&cnt_crc)             St <= StIPG;
        StIPG     : if (cnt_pause == 'd11)    St <= StIdle;
        default   : St <= StIdle;
      endcase
    end
  end

  // Counters
  always_ff@(posedge iclk) begin
    if (St == StIdle)   cnt_req   <= cnt_req + 1'b1;
    else                cnt_req   <= '0;

    if (St == StPream)  cnt_pream <= cnt_pream + 1'b1;
    else                cnt_pream <= '0;

    if (St == StDat)    cnt_dat   <= cnt_dat + 1'b1;
    else                cnt_dat   <= '0;

    if (St == StCRC)    cnt_crc   <= cnt_crc + 1'b1;
    else                cnt_crc   <= '0;

    if (St == StIPG)    cnt_pause <= cnt_pause + 1'b1;
    else                cnt_pause <= '0;
  end
  
  // Data select
  always_ff@(posedge iclk) begin
    if (irst)                  ddat <= '0;
    else if (iena)             ddat <= idat;
      else if (St == StPream)  ddat <= (cnt_pream >= 5'd15) ? (pSFD) : (8'h55);

    if (ieop | feop)  ena <= 1'b0;
    else              ena <= (St == StPream) | (St == StDat) | (St == StCRC);
    dena <= ena;
  end
  
  // CRC32 calculating
  always_ff@(posedge iclk) begin
    if (iena)                 crc <= eth_crc32_8d(crc[31:0], idat);
    else if (St == StCRC)     crc <= (crc << 8) | 8'hFF;
      else if (St == StIdle)  crc <= '1;
  end
  
  // IEEE std. CRC inversion
  assign crc_inv = ~{crc[24], crc[25], crc[26], crc[27], crc[28], crc[29], crc[30], crc[31]};
  
  // Prefifo data mux
  assign predat = (St == StCRC) ? (crc_inv) : (ddat);
  // S/P
  always_ff@(posedge iclk) begin
    if (ena)  sdr_cnt <= sdr_cnt + 1'b1;
    else      sdr_cnt <= '0;
  end
  assign adat = (sdr_cnt) ? (predat[7:4]) : (predat[3:0]);
  
  //--------------------------------------------------------------------------------------------------------
  // Output data
  //--------------------------------------------------------------------------------------------------------
  
  assign obsy = (St != StIdle);
  assign oreq = ((St == StDat) | (cnt_pream > 5'd12) | (&cnt_req)) & ~sdr_cnt;

  assign odat = ddio__dataout[3:0];
  assign oena = ddio__dataout[4];
  assign oclk = ddio_clk__dataout;

  //--------------------------------------------------------------------------------------------------------
  // Modules
  //--------------------------------------------------------------------------------------------------------

  // DDR Outputs
  assign ddio__aclr         = irst;
  assign ddio__dat_h        = { ena, adat};
  assign ddio__dat_l        = { ena, adat};
  assign ddio__outclock     = iclk;
  // DDR clock out
  assign ddio_clk__aclr     = irst;
  assign ddio_clk__dat_h    = 1'b1;
  assign ddio_clk__dat_l    = 1'b0;
  assign ddio_clk__outclock = iclk;
  
  altddio_out
  #(
    . extend_oe_disable       ("OFF"              ) ,
    . invert_output           ("OFF"              ) ,
    . intended_device_family  ("Cyclone V"        ) ,
    . lpm_hint                ("UNUSED"           ) ,
    . lpm_type                ("altddio_out"      ) ,
    . oe_reg                  ("UNREGISTERED"     ) ,
    . power_up_high           ("OFF"              ) ,
    . width                   (pW_DDIO            )
  )
  ddio__
  (
    . aclr                    (ddio__aclr         ) ,
    . dataout                 (ddio__dataout      ) ,
    . outclock                (ddio__outclock     ) ,
    . datain_h                (ddio__dat_h        ) ,
    . datain_l                (ddio__dat_l        ) ,
    . aset                    (1'b0               ) ,
    . oe                      (1'b1               ) ,
    . outclocken              (1'b1               ) ,
    . sclr                    (1'b0               ) ,
    . sset                    (1'b0               ) ,
    . oe_out                  ( )
  );
  
  altddio_out
  #(
    . extend_oe_disable       ("OFF"              ) ,
    . invert_output           ("OFF"              ) ,
    . intended_device_family  ("Cyclone V"        ) ,
    . lpm_hint                ("UNUSED"           ) ,
    . lpm_type                ("altddio_out"      ) ,
    . oe_reg                  ("UNREGISTERED"     ) ,
    . power_up_high           ("OFF"              ) ,
    . width                   (1                  )
  )
  ddio_clk__
  (
    . aclr                    (ddio_clk__aclr     ) ,
    . dataout                 (ddio_clk__dataout  ) ,
    . outclock                (ddio_clk__outclock ) ,
    . datain_h                (ddio_clk__dat_h    ) ,
    . datain_l                (ddio_clk__dat_l    ) ,
    . aset                    (1'b0               ) ,
    . oe                      (1'b1               ) ,
    . outclocken              (1'b1               ) ,
    . sclr                    (1'b0               ) ,
    . sset                    (1'b0               ) ,
    . oe_out                  ( )
  );
  
endmodule
