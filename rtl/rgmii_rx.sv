
//
// Project       : RGMII core
// Author        : Borchsh Vladislav
// Contacts      : borsch.v@micran.ru
// Workfile      : rgmii_rx.sv
// Description   : Receiving module of RGMII interface
//

module rgmii_rx
  (
    input             iclk,
    input             irst,
    input             iena,
    input       [3:0] idat,
    //
    output            osop,
    output            oena,
    output      [7:0] odat,
    output            oeop
  );

  //--------------------------------------------------------------------------------------------------
  // Declaration parameters
  //--------------------------------------------------------------------------------------------------

  localparam pSFD   = 8'hD5;

  //--------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------
  
  logic         [2:0] val_in;
  logic         [3:0] dat_in [2];
  logic         [7:0] pre_dat;
  logic               cnt;
  logic               ena_in;
  logic               sop_in;

  //--------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------
  
  always_ff@(posedge iclk) begin
    val_in  <= val_in << 1 | iena;
    dat_in  <= '{idat, dat_in[0]};
    pre_dat <= {dat_in[1], pre_dat[7:4]};
    sop_in  <= val_in[2] & (pre_dat == pSFD) & ~ena_in;
    //
    if (!val_in[2])   ena_in <= 1'b0;
    else if (sop_in)  ena_in <= 1'b1;
    //
    if (ena_in) cnt <= cnt + 1'b1;
    else        cnt <= '1;
  end

  //--------------------------------------------------------------------------------------------------
  // Output data
  //--------------------------------------------------------------------------------------------------
  
  assign osop = sop_in;
  assign oeop = ena_in & (!val_in[2]);
  assign oena = ena_in & cnt;
  assign odat = pre_dat;

endmodule
