
//
// Project       : RGMII core
// Author        : Borchsh Vladislav
// Contacts      : borsch.v@micran.ru
// Workfile      : rgmii_top.sv
// Description   : Top level of RGMII PHY MAC
//

module rgmii_top
  #(
    parameter pBUF_SIZE = "ext"
  )
  (
    input             iclkin   ,
    input             iclkout  ,
    input             iclk     , // 25 MHz
    input             iclk_tx  , // 25 MHz *180
    input             irst     ,
    input             isop     ,
    input             iena     ,
    input       [7:0] idat     ,
    input             ieop     ,
    output            osop     ,
    output            oena     ,
    output      [7:0] odat     ,
    output            oeop     ,
    output            otx_rdy  ,
    output            otx_busy ,
    // PHY
    input             iphy_nint,
    input             iphy_clk ,
    input             iphy_ena ,
    input       [3:0] iphy_dat ,
    output            ophy_ena ,
    output      [3:0] ophy_dat ,
    output            ophy_clk ,
    input             nint     ,
    output            mdc      ,
    output            reset    ,
    output            cfg      ,
    inout             mdio     ,
    inout             ptp
  );
  
  //--------------------------------------------------------------------------------------------------
  // Declaration parameters
  //--------------------------------------------------------------------------------------------------
  
  localparam pUSE_CRC = "false"; // Delete CRC in rx/tx packet? true/false

  //--------------------------------------------------------------------------------------------------
  // Declaration modules wires
  //--------------------------------------------------------------------------------------------------
  
  logic                 src__iclkin      ;
  logic                 src__iclkout     ;
  logic                 src__irst        ;
  logic                 src__ireq        ;
  logic                 src__isop        ;
  logic                 src__iena        ;
  logic           [7:0] src__idat        ;
  logic                 src__ieop        ;
  logic                 src__osop        ;
  logic                 src__oena        ;
  logic           [7:0] src__odat        ;
  logic                 src__oeop        ;
  logic                 src__ordy        ;
  logic                 src__oerr        ;

  logic                 tx__iclk         ;
  logic                 tx__iclk_tx      ;
  logic                 tx__irst         ;
  logic                 tx__isop         ;
  logic                 tx__iena         ;
  logic           [7:0] tx__idat         ;
  logic                 tx__ieop         ;
  logic                 tx__oena         ;
  logic           [3:0] tx__odat         ;
  logic                 tx__oclk         ;
  logic                 tx__obsy         ;
  logic                 tx__oreq         ;

  logic                 rx__iclk         ;
  logic                 rx__irst         ;
  logic                 rx__iena         ;
  logic           [3:0] rx__idat         ;
  logic                 rx__osop         ;
  logic                 rx__oena         ;
  logic           [7:0] rx__odat         ;
  logic                 rx__oeop         ;

  logic                 sink__iclkin     ;
  logic                 sink__iclkout    ;
  logic                 sink__irst       ;
  logic                 sink__ireq       ;
  logic                 sink__isop       ;
  logic                 sink__iena       ;
  logic           [7:0] sink__idat       ;
  logic                 sink__ieop       ;
  logic                 sink__osop       ;
  logic                 sink__oena       ;
  logic           [7:0] sink__odat       ;
  logic                 sink__oeop       ;
  logic                 sink__oerr       ;
  logic                 sink__ordy       ;

  logic                 mgb__iclk        ;
  logic                 mgb__irst        ;
  logic                 mgb__inint       ;
  logic                 mgb__ocfg        ;
  logic                 mgb__ptp         ;
  logic                 mgb__omdc        ;
  logic                 mgb__mdio        ;
  logic                 mgb__oreset      ;
  
  //--------------------------------------------------------------------------------------------------
  // Declaration variables
  //--------------------------------------------------------------------------------------------------

  logic          clk_ena;

  //--------------------------------------------------------------------------------------------------
  // BODY
  //--------------------------------------------------------------------------------------------------

  // Make 12.5 MHz
  always@(posedge iclk_tx) begin
    clk_ena <= clk_ena + 1'b1;
  end

  //--------------------------------------------------------------------------------------------------
  // Output signals
  //--------------------------------------------------------------------------------------------------
  
  assign ophy_ena = tx__oena;
  assign ophy_dat = tx__odat;
  assign ophy_clk = tx__oclk;
  assign otx_busy = tx__obsy;
  assign otx_rdy  = src__ordy;

  assign mdc      = mgb__omdc;
  assign reset    = mgb__oreset;
  assign cfg      = mgb__ocfg;
  
  assign osop     = sink__osop;
  assign oena     = sink__oena;
  assign odat     = sink__odat;
  assign oeop     = sink__oeop;
  
  //--------------------------------------------------------------------------------------------------
  // Modules
  //--------------------------------------------------------------------------------------------------

  assign src__iclkin     = iclkin;
  assign src__iclkout    = iclk;
  assign src__irst       = irst | src__oerr;
  assign src__ireq       = tx__oreq;
  assign src__isop       = isop;
  assign src__iena       = iena;
  assign src__idat       = idat;
  assign src__ieop       = ieop;

  assign tx__iclk        = iclk;
  assign tx__iclk_tx     = iclk_tx;
  assign tx__irst        = irst;
  assign tx__isop        = src__osop;
  assign tx__iena        = src__oena;
  assign tx__idat        = src__odat;
  assign tx__ieop        = src__oeop;

  assign rx__iclk        = iphy_clk;
  assign rx__irst        = irst;
  assign rx__iena        = iphy_ena;
  assign rx__idat        = iphy_dat;

  assign sink__iclkin    = iphy_clk;
  assign sink__iclkout   = iclkin;
  assign sink__ireq      = 1'b1;
  assign sink__irst      = irst | sink__oerr;
  assign sink__isop      = rx__osop;
  assign sink__iena      = rx__oena;
  assign sink__idat      = rx__odat;
  assign sink__ieop      = rx__oeop;

  assign mgb__iclk       = iclk;
  assign mgb__irst       = irst;
  assign mgb__inint      = nint;

  rgmii_src
  #(
    . pWORDS       (pBUF_SIZE         )
  )
  src__
  (
    . iclkin       (src__iclkin       ) ,
    . iclkout      (src__iclkout      ) ,
    . ireq         (src__ireq         ) ,
    . irst         (src__irst         ) ,
    . isop         (src__isop         ) ,
    . iena         (src__iena         ) ,
    . idat         (src__idat         ) ,
    . ieop         (src__ieop         ) ,
    . osop         (src__osop         ) ,
    . oena         (src__oena         ) ,
    . odat         (src__odat         ) ,
    . oeop         (src__oeop         ) ,
    . ordy         (src__ordy         ) ,
    . oerr         (src__oerr         )
  );
  
  rgmii_tx
  #(
    . pUSE_CRC    (pUSE_CRC           )
  )
  tx__
  (
    . iclk        (tx__iclk           ) ,
    . iclk_tx     (tx__iclk_tx        ) ,
    . irst        (tx__irst           ) ,
    . isop        (tx__isop           ) ,
    . iena        (tx__iena           ) ,
    . idat        (tx__idat           ) ,
    . ieop        (tx__ieop           ) ,
    . oena        (tx__oena           ) ,
    . odat        (tx__odat           ) ,
    . oclk        (tx__oclk           ) ,
    . obsy        (tx__obsy           ) ,
    . oreq        (tx__oreq           )
  );
  
  rgmii_rx
  rx__
  (
    . iclk        (rx__iclk           ) ,
    . irst        (rx__irst           ) ,
    . iena        (rx__iena           ) ,
    . idat        (rx__idat           ) ,
    . osop        (rx__osop           ) ,
    . oena        (rx__oena           ) ,
    . odat        (rx__odat           ) ,
    . oeop        (rx__oeop           )
  );
  
  rgmii_sink
  #(
    . pWORDS       (pBUF_SIZE         )
  )
  sink__
  (
    . iclkin       (sink__iclkin      ) ,
    . iclkout      (sink__iclkout     ) ,
    . irst         (sink__irst        ) ,
    . ireq         (sink__ireq        ) ,
    . isop         (sink__isop        ) ,
    . iena         (sink__iena        ) ,
    . idat         (sink__idat        ) ,
    . ieop         (sink__ieop        ) ,
    . osop         (sink__osop        ) ,
    . oena         (sink__oena        ) ,
    . odat         (sink__odat        ) ,
    . oeop         (sink__oeop        ) ,
    . ordy         (sink__ordy        ) ,
    . oerr         (sink__oerr        ) 
  );
  
  // Control interface to Eth PHY
  rgmii_mgb_ctrl
  mgb__
  (
    . iclk         (mgb__iclk         ) , // 50 MHz max
    . irst         (mgb__irst         ) ,
    . inint        (mgb__inint        ) ,
    . ocfg         (mgb__ocfg         ) ,  
    . ptp          (ptp               ) ,
    . omdc         (mgb__omdc         ) ,
    . mdio         (mdio              ) ,
    . oreset       (mgb__oreset       )
  );

endmodule
